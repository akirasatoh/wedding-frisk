/**
  Wedding Frisk Project
   (c) Akira Satoh
**/

extern "C" {
#include "user_interface.h"
} //ESP-WROOM-02でアナログ入力をするために必要

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "FS.h"
//時計
#include <Time.h>
#include <TimeLib.h>
#include <NTP.h>
// OLEDライブラリ
#include <Wire.h>
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "OLEDDisplayUi.h"
// Include custom images
#include "images.h"
//Frame設定
#include "Frames_setting.h"
//タイマー割り込み
#include <Ticker.h>
//Wi-Fi設定
#include "WiFi_setting.h"
//天気設定
#include "Tenki_setting.h"

SSD1306  display(0x3c, 4, 5); // Initialize the OLED display using Wire library
OLEDDisplayUi ui (&display);

char PageTitle[64];

//オーバーレイ一覧
void StatusBarOverlay(OLEDDisplay *display, OLEDDisplayUiState* state);
void NoOverlay(OLEDDisplay *display, OLEDDisplayUiState* state);
//フレーム一覧
void Boot_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void Wait_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void Input_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) ;
void Menu_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void SwitchTest_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void analogClock_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);

FrameCallback frames[1] = { Boot_Frame };
OverlayCallback overlays[1] = { StatusBarOverlay };

bool isWifiConnected = false;

//タイマー割り込み
Ticker ticker_WiFi;

//Wi-Fi接続管理 0:タイマーで接続監視 1:メインループで接続を試す 2:設定中
uint8_t doWiFiConnect = 0;
uint8_t doStartNTP = 0;
void wifiCheck() {
  if (doWiFiConnect == 0) {
    int current_status = statusWiFi();
    Serial.print("wifi status:");
    Serial.println(current_status);
    /*
      WL_IDLE_STATUS:0
      WL_NO_SSID_AVAIL:1
      WL_SCAN_COMPLETED:2
      WL_CONNECTED:3
      WL_CONNECT_FAILED:4
      WL_CONNECTION_LOST:5
      WL_DISCONNECTED:6
      WL_NO_SHIELD:255
    */

    if (current_status != WL_CONNECTED) {
      if (doWiFiConnect == 0) {
        Serial.println("wifiCheck - Wi-Fi isn't connect. try connect");
        isWifiConnected = false;
        doWiFiConnect = 1;
        return;
      }
    } else if (current_status == WL_CONNECTED) {
      if (isWifiConnected == false ) {
        isWifiConnected = true;
        doStartNTP = 1;
      }
    }
  }
  ticker_WiFi.once_ms(10000, wifiCheck);
}

void setup() {

  Serial.begin(115200);
  Serial.println("Booting");

  SPIFFS.begin();


  // Initialising the UI will init the display too.
  Serial.println("Initialize UI");
  ui.setTargetFPS(60);
  ui.disableAllIndicators(); //画面下部のページ位置表示無効
  ui.disableAutoTransition(); //自動ページ切り替え無効
  ui.init();
  display.flipScreenVertically();
  //ブート画面表示
  frames[0] = Boot_Frame;
  ui.setFrames(frames, 1);
  ui.update();

  startWiFi();
  getSavedWiFiList();
  if (connectSavedWiFi()) {
    isWifiConnected = true;
    ntp_begin(2390);
  }
  doWiFiConnect = 0;
  //searchWiFi();
  //connectWiFi("aristocrat-wifi", "akira1220");

  // ArduinoOTA.setPort(8266); // Port defaults to 8266
  // ArduinoOTA.setHostname("myesp8266"); // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setPassword((const char *)"123");// No authentication by default

  ArduinoOTA.onStart([]() {
    Serial.println("OTA Start");
    display.clear();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
    display.drawString(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2 - 10, "OTA Update");
    display.display();
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
    display.drawProgressBar(4, 32, 120, 8, progress / (total / 100) );
    display.display();
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nOTA End");
    display.clear();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
    display.drawString(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, "Restart");
    display.display();
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("OTA Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();


  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Add frames
  strncpy(PageTitle, "ﾒｲﾝﾒﾆｭｰ", 64);

  currentMenuItems = topMenuItems;
  frames[0] = Menu_Frame;
  //frames[0] = Input_Frame;
  ui.setOverlays(overlays, 1);

  //タイマー割り込みスタート
  ticker_WiFi.once_ms(2000, wifiCheck);
}


void loop() {
  ArduinoOTA.handle();

  KeyCheck();

  if (doWiFiConnect == 1) {
    connectSavedWiFiAsync();
    connectSavedWiFiAsyncCheck();
  }
  if (doStartNTP == 1) {
    ntp_begin(2390);
    doStartNTP = 0;
  }

  int remainingTimeBudget = ui.update();
  if (remainingTimeBudget > 0) {
    // You can do some work here
    // Don't do stuff if you are below your
    // time budget.
    delay(remainingTimeBudget);
  }
}



