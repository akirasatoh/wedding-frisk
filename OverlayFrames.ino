//ステータスバー
void StatusBarOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {
  time_t n = now();
  time_t t;
  char timestr1[9];
  char timestr2[9];
  // JST
  t = localtime(n, 9);
  if (year(t) > 2000) {
    sprintf(timestr1, "%02d/%02d/%02d", year(t) - 2000, month(t), day(t));
    sprintf(timestr2, "%02d:%02d:%02d", hour(t), minute(t), second(t));

    display->setTextAlignment(TEXT_ALIGN_RIGHT);
    display->setFont(ArialMT_Plain_10);
    display->drawString(128, 0, timestr1);
    display->drawString(128, 6, timestr2);
  }
  if (isWifiConnected) display->drawXbm(73, 3, WifiIcon_width, WifiIcon_height, WifiIcon_bits);
  PrintUtf8(display, PageTitle, 0, 0, WHITE);
}

//何も描画しない
void NoOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {

}

//エラー表示
void ErrorOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {
  display->fillRect(0, 32, 128, 48);
  PrintUtf8(display, "接続失敗", 32, 32, INVERSE);
  if (lastErrorMillis + 2000 < millis()) {
    overlays[0] = StatusBarOverlay;
  }
}
