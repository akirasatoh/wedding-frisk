
bool tenkiLoadCity() {
  File f = SPIFFS.open("/tenki.txt", "r");
  if (!f) {
    Serial.println("tenki No City Saved");
    SelectPrefIndex = 13;
    SelectCityIndex = 0;
    SelectCityName = String(citynames[SelectPrefIndex][SelectCityIndex]);
    Serial.print("tenki Auto Select : ");
    Serial.println(citynames[SelectPrefIndex][SelectCityIndex]);
    return false;
  }
  String tmp = f.readStringUntil('\n');
  tmp.trim();
  SelectPrefIndex = tmp.toInt();
  tmp = f.readStringUntil('\n');
  tmp.trim();
  SelectCityIndex = tmp.toInt();
  Serial.print("tenki Select : ");
  Serial.println(citynames[SelectPrefIndex][SelectCityIndex]);
  SelectCityName = String(citynames[SelectPrefIndex][SelectCityIndex]);
  return true;
}

bool tenkiSaveCity() {
  File f = SPIFFS.open("/tenki.txt", "w");
  if (!f) {
    Serial.println("tenki file open failed");
    return false;
  }
  f.println(SelectPrefIndex);
  f.println(SelectCityIndex);
  f.close();
  return true;
}

bool tenkiDownloadData() {

  Serial.println("Start Tenki Data Download");
  String url = "http://pic8pin.dip.jp/wf/tenkijson.php?city=" + String(citycodes[SelectPrefIndex][SelectCityIndex]);
  if (isWiFiConnect() == false ) {
    TodayTenki = "WiFi接続";
    TomorrowTenki = "して下さ";
    TodayTemperatureMax = "い";
    TodayTemperatureMin = "";
    TomorrowTemperatureMax = "";
    TomorrowTemperatureMin = "";
    tenkiScrollMax = 3;
    return false;
  }
  tenkiResBody = wget(url);
  Serial.println("---downloaded:---------");
  Serial.println(tenkiResBody);
  Serial.println("-----------------------");
  if (tenkiResBody == "") {
    TodayTenki = "取得に失";
    TomorrowTenki = "敗しまし";
    TodayTemperatureMax = "た";
    TodayTemperatureMin = "";
    TomorrowTemperatureMax = "";
    TomorrowTemperatureMin = "";
    tenkiScrollMax = 3;
    return false;
  } else {
    TodayTenki = Split(tenkiResBody, ',', 1);
    TodayTemperatureMax = Split(tenkiResBody, ',', 2);
    TodayTemperatureMin = Split(tenkiResBody, ',', 3);
    TomorrowTenki = Split(tenkiResBody, ',', 4);
    TomorrowTemperatureMax = Split(tenkiResBody, ',', 5);
    TomorrowTemperatureMin = Split(tenkiResBody, ',', 6);
    tenkiScrollMax = 7;
    while (Split(tenkiResBody, ',', tenkiScrollMax++) != "END") {}
    tenkiScrollMax -= 5;
    Serial.print("tenkiScrollMax:");
    Serial.println(tenkiScrollMax);
    return true;
  }
}
String Split(String data, char separator, int index) {
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;
  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  String ret = found > index ? data.substring(strIndex[0], strIndex[1]) : "";
  if (ret == String(separator)) ret = "";
  return ret;
}

