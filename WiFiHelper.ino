void disconnectWiFi() {
  WiFi.disconnect();
}

void startWiFi() {
  //Wi-Fiスタート
  WiFi.mode(WIFI_STA);
  disconnectWiFi();
}

String statusWiFiStr() {
  switch (statusWiFi()) {
    case WL_IDLE_STATUS:
      return "アイドル";
    case WL_NO_SSID_AVAIL:
      return "有効なSSIDなし";
    case WL_SCAN_COMPLETED:
      return "スキャン完了";
    case WL_CONNECTED:
      return "接続完了";
    case WL_CONNECT_FAILED:
      return "接続失敗";
    case WL_CONNECTION_LOST:
      return "接続を失いました";
    case WL_DISCONNECTED:
      return "切断";
    case WL_NO_SHIELD:
      return "ハードエラー";
    default:
      return "不明";
  }
}

void printWiFiList(int n) {
  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
}

int searchWiFi() {
  Serial.println("scan start");
  // WiFi.scanNetworks will return the number of networks found
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  printWiFiList(n);
  return n;
}

int statusWiFi() {
  return WiFi.status();
}

bool isWiFiConnect() {
  switch (statusWiFi()) {
    case WL_CONNECTED:
      return true;
    case WL_NO_SSID_AVAIL:
    case WL_CONNECT_FAILED:
    case WL_IDLE_STATUS:
    case WL_DISCONNECTED:
    default:
      return false;
  }
}

bool saveWiFi(String ssid, String password) {
  // open file for writing
  String filepath = "/wifi/" + ssid;
  File f = SPIFFS.open(filepath, "w");
  if (!f) {
    Serial.print("file ");
    Serial.print(filepath);
    Serial.println("open failed");
    return false;
  }
  f.println(password);
  f.close();
  return true;
}

bool removeWiFi(String ssid) {
  if (isWiFiConnect() && WiFi.SSID() == ssid) {
    disconnectWiFi();
  }
  String filepath = "/wifi/" + ssid;
  if (SPIFFS.exists(filepath)) {
    SPIFFS.remove(filepath);
    return true;
  }
  return false;
}

bool connectWiFi(String ssid, String password) {
  char cssid[256];
  char cpassword[256];
  ssid.trim();
  password.trim();
  sprintf(cssid, "%s", ssid.c_str());
  sprintf(cpassword, "%s", password.c_str());
  password.toCharArray(cpassword, 256);
  Serial.print("Connect to ");
  Serial.print(cssid);
  Serial.print("[");
  Serial.print(cpassword);
  Serial.println("]");
  startWiFi();
  WiFi.begin(cssid, cpassword);
  if (WiFi.waitForConnectResult() == WL_CONNECTED) {
    saveWiFi(ssid, password);
    Serial.print("Connection Success to ");
    Serial.println(ssid);
    return true;
  } else {
    Serial.print("Connection Failed to ");
    Serial.println(ssid);
    return false;
  }
}


bool connectWiFiAsync(String ssid, String password) {
  char cssid[256];
  char cpassword[256];
  ssid.trim();
  password.trim();
  sprintf(cssid, "%s", ssid.c_str());
  sprintf(cpassword, "%s", password.c_str());
  password.toCharArray(cpassword, 256);
  Serial.print("Connect to ");
  Serial.print(cssid);
  Serial.print("[");
  Serial.print(cpassword);
  Serial.println("] Async");
  startWiFi();
  WiFi.begin(cssid, cpassword);
  return true;
}

uint8_t getSavedWiFiList() {
  String str = "";
  uint8_t count = 0;
  Dir dir = SPIFFS.openDir("/wifi/");
  while (dir.next()) {
    ssidList[count] = String(dir.fileName()).substring(6);
    File f = dir.openFile("r");
    passwordList[count] = String(f.readStringUntil('\n'));
    f.close();
    count++;
  }
  ssidCount = count;
  return count;
}

int indexSSID(String ssid) {
  for (int i = 0; i < ssidCount; i++) {
    if (ssidList[i] == ssid) return i;
  }
  return -1;
}

bool connectSavedWiFi() {
  Serial.println("connectSavedWiFi");
  if (ssidCount > 0) {
    int n = searchWiFi();
    if (n > 0) {
      String ssids[8];
      int rssis[8];
      for (int i = 0; i < n && i <= 8; i++) {
        ssids[i] = WiFi.SSID(i);
        rssis[i] = WiFi.RSSI(i);
      }
      //電波強度順にソートして、失敗しても次のWiFiを試す
      for (int x = 0; x < n; x++) {
        for (int y = 0; y < n - 1; y++) {
          if (rssis[y] < rssis[y + 1]) {
            int rssiholder = rssis[y + 1];
            rssis[y + 1] = rssis[y];
            rssis[y] = rssiholder;
            String ssidholder = ssids[y + 1];
            ssids[y + 1] = ssids[y];
            ssids[y] = ssidholder;
          }
        }
      }
      //電波強度の強い順にパスワードが保存されていれば接続を試みる
      for (int i = 0; i < n; i++) {
        int ssidIndex = indexSSID(ssids[i]);
        if (ssidIndex >= 0) {
          if (connectWiFi(ssidList[ssidIndex], passwordList[ssidIndex])) {
            return true;
          }
        }
      }
    }
  }
  return false;
}

bool isScanning = false;

void connectSavedWiFiAsyncCheck() {
  if (isScanning == true) {
    int n = WiFi.scanComplete();
    if (n > 0 ) {
      printWiFiList(n);
      if (n > 0) {
        String ssids[8];
        int rssis[8];
        for (int i = 0; i < n && i <= 8; i++) {
          ssids[i] = WiFi.SSID(i);
          rssis[i] = WiFi.RSSI(i);
        }
        //電波強度順にソートして、失敗しても次のWiFiを試す
        for (int x = 0; x < n; x++) {
          for (int y = 0; y < n - 1; y++) {
            if (rssis[y] < rssis[y + 1]) {
              int rssiholder = rssis[y + 1];
              rssis[y + 1] = rssis[y];
              rssis[y] = rssiholder;
              String ssidholder = ssids[y + 1];
              ssids[y + 1] = ssids[y];
              ssids[y] = ssidholder;
            }
          }
        }
        //電波強度の強い順にパスワードが保存されていれば接続を試みる
        for (int i = 0; i < n; i++) {
          int ssidIndex = indexSSID(ssids[i]);
          if (ssidIndex >= 0) {
            //if (connectWiFi(ssidList[ssidIndex], passwordList[ssidIndex])) {
            connectWiFiAsync(ssidList[ssidIndex], passwordList[ssidIndex]);
            break;
            //}
          }
        }
      }
      isScanning = false;
      doWiFiConnect = 0;
      ticker_WiFi.once_ms(10000, wifiCheck);
    }
  }
}

void connectSavedWiFiAsync() {
  if (isScanning == false) {
    Serial.println("connectSavedWiFi");
    if (ssidCount > 0) {
      startWiFi();
      isScanning = true;
      Serial.println("startScanningSavedWiFi");
      WiFi.scanNetworks(true);
    }
  }
}


