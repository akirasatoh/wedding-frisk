//variable
uint8_t ssidCount = 0;
String ssidList[8];
String passwordList[8];

//functions
void disconnectWiFi();
void startWiFi();
String statusWiFiStr();
int searchWiFi();
int statusWiFi();
bool isWiFiConnect();
bool saveWiFi(String ssid, String password);
bool removeWiFi(String ssid);
bool connectWiFi(String ssid, String password);
uint8_t getSavedWiFiList();
int indexSSID(String ssid);
bool connectSavedWiFi();
