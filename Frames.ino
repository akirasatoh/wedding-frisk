//日本語描画：
//PrintUtf8(display, "日本語あいうえお", 0, 0, WHITE);
//高速英語描画：
//display->drawString(x+0, y+0, "EnglishABC" + String(digitalRead(0) ? "0" : "1"));

//ブート画面
void Boot_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  display->drawXbm(x + 26, y + 2, WeddingFrisk_Logo_width, WeddingFrisk_Logo_height, WeddingFrisk_Logo_bits);
  PrintUtf8(display, "起動中です.....", x + 0, y + 48, WHITE);
}

//処理中画面
void Wait_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  PrintUtf8(display, "処理中です", x + 0, y + 16, WHITE);
  PrintUtf8(display, "お待ちください", x + 0, y + 32, WHITE);
}

//メニュー
void Menu_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  uint8_t i;
  for (i = viewableItemIndex; i < allItemsCount ; i++) {
    if (i - viewableItemIndex >= 3) break;
    uint8_t drawy = 16 + (i - viewableItemIndex) * 16;
    if (i == selectedItemIndex) {
      display->fillRect(x + 0, y + drawy, 128, 16);
      PrintUtf8(display, currentMenuItems[i], x + 0, y + drawy, INVERSE);
    } else {
      PrintUtf8(display, currentMenuItems[i], x + 0, y + drawy, WHITE);
    }
  }
}

//天気表示画面
void Tenki_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  if (selectedItemIndex == 0) {
    PrintUtf8(display, "今日", x + 16, y + 16, WHITE);
    PrintUtf8(display, "明日", x + 80, y + 16, WHITE);
  }
  if (selectedItemIndex <= 1) {
    PrintUtf8(display, TodayTenki, x + ((TodayTenki.length() / 3 <= 2) ? 16 : 0), y + 32 - selectedItemIndex * 16, WHITE);
    PrintUtf8(display, TomorrowTenki, x + ((TomorrowTenki.length() / 3 <= 2) ? 16 : 0) + 64, y + 32 - selectedItemIndex * 16, WHITE);
  }
  if (selectedItemIndex <= 2) {
    PrintUtf8(display, TodayTemperatureMax, x + 8, y + 48 - selectedItemIndex * 16, WHITE);
    PrintUtf8(display, "/", x + 24, y + 48 - selectedItemIndex * 16, WHITE);
    PrintUtf8(display,  TodayTemperatureMin, x + 32, y + 48 - selectedItemIndex * 16, WHITE);
    PrintUtf8(display, TomorrowTemperatureMax, x + 80, y + 48 - selectedItemIndex * 16, WHITE);
    PrintUtf8(display, "/", x + 96, y + 48 - selectedItemIndex * 16, WHITE);
    PrintUtf8(display, TomorrowTemperatureMin, x + 104, y + 48 - selectedItemIndex * 16, WHITE);
    for (int i = 16; i < 64 - 16 * selectedItemIndex; i += 2) {
      display->setPixel(x + 64, y + i);
    }
  }
  if (selectedItemIndex > 0 && tenkiScrollMax > 3) {
    int starti = selectedItemIndex - 2;
    if (starti < 1) starti = 1;
    int drawY = 0;
    if (selectedItemIndex == 1) drawY += 32;
    if (selectedItemIndex == 2) drawY += 16;
    for (int i = 0; i < 3; i ++) {
      drawY += 16;
      if (drawY >= 64) break;
      String drawStr = Split(tenkiResBody, ',', 6 + starti + i);
      if (drawStr == "END") break;
      PrintUtf8(display, drawStr, x + 0, y + drawY, WHITE);
    }
  }
}

//文字入力画面
void Input_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  drawKeyboard(display, state, x, y);
}

//スイッチテストページ
uint8_t adcList[256];
uint8_t adcListStart = 0;
void SwitchTest_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  adcList[adcListStart] = 16 + (47 - (system_adc_read() / 22));
  int16_t x0 = 128;
  int16_t y0;
  int16_t x1;
  int16_t y1;
  for (int i = 0; i < 127; i++) {
    x1 = 127 - i;
    y1 = adcList[(uint8_t)(adcListStart - i)];
    x0 = x1;
    y0 = adcList[(uint8_t)(adcListStart - i - 1)];
    display->drawLine(x + x1, y + y1, x + x0, y + y0);
  }
  //しきい値
  int16_t yA = 16 + (47 - (902 / 22));
  int16_t yB = 16 + (47 - (672 / 22));
  int16_t yC = 16 + (47 - (434 / 22));
  int16_t yD = 16 + (47 - (152 / 22));
  for (int i = 0; i < 127; i += 3) {
    display->setPixel(x + i, y + yA);
    display->setPixel(x + i, y + yB);
    display->setPixel(x + i, y + yC);
    display->setPixel(x + i, y + yD);
  }
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(x + 0, y + 32, "SW5:" + String(digitalRead(0) ? "0" : "1"));
  display->drawString(x + 0, y + 48, "ADC" + String(system_adc_read()));
  adcListStart++;
}

//画面焼き付き解消
uint8_t isRepairWhite = 0;
void OLEDRepair_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  if (isRepairWhite++ < 3) {
    display->fillRect(x + 0, y + 0, 128, 64);
  } else if (isRepairWhite > 5) {
    isRepairWhite = 0;
  }
}

//アナログ時計
void analogClock_Frame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  int screenW = 128;
  int screenH = 64;
  int clockCenterX = 23;
  int clockCenterY = ((screenH - 16) / 2) + 16; // top yellow part is 16 px height
  int clockRadius = 23;
  time_t n = now();
  time_t t = localtime(n, 9);
  if (year(t) > 2000) {
    // Draw the clock face
    //  display->drawCircle(clockCenterX + x, clockCenterY + y, clockRadius);
    display->drawCircle(clockCenterX + x, clockCenterY + y, 2);
    //
    //hour ticks
    for ( int z = 0; z < 360; z = z + 30 ) {
      //Begin at 0deg and stop at 360deg
      float angle = z ;
      angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
      int x2 = ( clockCenterX + ( sin(angle) * clockRadius ) );
      int y2 = ( clockCenterY - ( cos(angle) * clockRadius ) );
      int x3 = ( clockCenterX + ( sin(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
      int y3 = ( clockCenterY - ( cos(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
      display->drawLine( x2 + x , y2 + y , x3 + x , y3 + y);
    }

    // display second hand
    float angle = second(t) * 6 ;
    angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
    int x3 = ( clockCenterX + ( sin(angle) * ( clockRadius - ( clockRadius / 5 ) ) ) );
    int y3 = ( clockCenterY - ( cos(angle) * ( clockRadius - ( clockRadius / 5 ) ) ) );
    display->drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y);
    //
    // display minute hand
    angle = minute(t) * 6 ;
    angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
    x3 = ( clockCenterX + ( sin(angle) * ( clockRadius - ( clockRadius / 4 ) ) ) );
    y3 = ( clockCenterY - ( cos(angle) * ( clockRadius - ( clockRadius / 4 ) ) ) );
    display->drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y);
    //
    // display hour hand
    angle = hour(t) * 30 + int( ( minute(t) / 12 ) * 6 )   ;
    angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
    x3 = ( clockCenterX + ( sin(angle) * ( clockRadius - ( clockRadius / 2 ) ) ) );
    y3 = ( clockCenterY - ( cos(angle) * ( clockRadius - ( clockRadius / 2 ) ) ) );
    display->drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y);

    char timestr1[9];
    char timestr2[9];
    sprintf(timestr1, "%04d/%02d/%02d", year(t), month(t), day(t));
    sprintf(timestr2, "%02d:%02d:%02d", hour(t), minute(t), second(t));
    display->setTextAlignment(TEXT_ALIGN_RIGHT);
    display->setFont(ArialMT_Plain_24);
    display->drawString(x + 128, y + 41, timestr2);
    PrintUtf8(display, timestr1, x + 48, y + 16, WHITE);
  } else {
    PrintUtf8(display, "時計を使用するに", x + 0, y + 16, WHITE);
    PrintUtf8(display, "はWiFiに接続する", x + 0, y + 32, WHITE);
    PrintUtf8(display, "必要があります", x + 0, y + 48, WHITE);
  }
}

