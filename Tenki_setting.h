char pref_name[][13] = {"北海道",
                        "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県",
                        "茨城県", "栃木県", "群馬県", "埼玉県", "千葉県", "東京都", "神奈川県",
                        "新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県", "静岡県", "愛知県",
                        "三重県", "滋賀県", "京都府", "大阪府", "兵庫県", "奈良県", "和歌山県",
                        "鳥取県", "島根県", "岡山県", "広島県", "山口県",
                        "徳島県", "香川県", "愛媛県", "高知県",
                        "福岡県", "佐賀県", "長崎県", "熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県"
                       };

char Hokkaido_cityname[][13] =  {"稚内", "旭川", "留萌", "網走", "北見", "紋別", "根室", "釧路", "帯広", "室蘭", "浦河", "札幌", "岩見沢", "倶知安", "函館", "江差"};
char Aomori_cityname[][13] =    {"青森", "むつ", "八戸"};
char Iwate_cityname[][13] =     {"盛岡", "宮古", "大船渡"};
char Miyagi_cityname[][13] =    {"仙台", "白石"};
char Akita_cityname[][13] =     {"秋田", "横手"};
char Yamagata_cityname[][13] =  {"山形", "米沢", "酒田", "新庄"};
char Fukushima_cityname[][13] = {"福島", "小名浜", "若松"};
char Ibaraki_cityname[][13] =   {"水戸", "土浦"};
char Tochigi_cityname[][13] =   {"宇都宮", "大田原"};
char Gunma_cityname[][13] =     {"前橋", "みなかみ"};
char Saitama_cityname[][13] =   {"さいたま", "熊谷", "秩父"};
char Chiba_cityname[][13] =     {"千葉", "銚子", "館山"};
char Tokyo_cityname[][13] =     {"東京", "大島", "八丈島", "父島"};
char Kanagawa_cityname[][13] =  {"横浜", "小田原"};
char Nigata_cityname[][13] =    {"新潟", "長岡", "高田", "相川"};
char Toyama_cityname[][13] =    {"富山", "伏木"};
char Ishikawa_cityname[][13] =  {"金沢", "輪島"};
char Fukui_cityname[][13] =     {"福井", "敦賀"};
char Yamanashi_cityname[][13] = {"甲府", "河口湖"};
char Nagano_cityname[][13] =    {"長野", "松本", "飯田"};
char Gifu_cityname[][13] =      {"岐阜", "高山"};
char Shizuoka_cityname[][13] =  {"静岡", "網代", "三島", "浜松"};
char Aichi_cityname[][13] =     {"名古屋", "豊橋"};
char Mie_cityname[][13] =       {"津", "尾鷲"};
char Shiga_cityname[][13] =     {"大津", "彦根"};
char Kyoto_cityname[][13] =     {"京都", "舞鶴"};
char Osaka_cityname[][13] =     {"大阪"};
char Hyogo_cityname[][13] =     {"神戸", "豊岡"};
char Nara_cityname[][13] =      {"奈良", "風屋"};
char Wakayama_cityname[][13] =  {"和歌山", "潮岬"};
char Tottori_cityname[][13] =   {"鳥取", "米子"};
char Shimane_cityname[][13] =   {"松江", "浜田", "西郷"};
char Okayama_cityname[][13] =   {"岡山", "津山"};
char Hiroshima_cityname[][13] = {"広島", "庄原"};
char Yamaguchi_cityname[][13] = {"下関", "山口", "柳井", "萩"};
char Tokushima_cityname[][13] = {"徳島", "日和佐"};
char Kagawa_cityname[][13] =    {"高松"};
char Ehime_cityname[][13] =     {"松山", "新居浜", "宇和島"};
char Kochi_cityname[][13] =     {"高知", "室戸岬", "清水"};
char Fukuoka_cityname[][13] =   {"福岡", "八幡", "飯塚", "久留米"};
char Saga_cityname[][13] =      {"佐賀", "伊万里"};
char Nagasaki_cityname[][13] =  {"長崎", "佐世保", "厳原", "福江"};
char Kumamoto_cityname[][13] =  {"熊本", "阿蘇乙姫", "牛深", "人吉"};
char Oita_cityname[][13] =      {"大分", "中津", "日田", "佐伯"};
char Miyazaki_cityname[][13] =  {"宮崎", "延岡", "都城", "高千穂"};
char Kagoshima_cityname[][13] = {"鹿児島", "鹿屋", "種子島", "名瀬"};
char Okinawa_cityname[][13] =   {"那覇", "名護", "久米島", "南大東", "宮古島", "石垣島", "与那国島"};

char (*citynames[])[13] = {Hokkaido_cityname, Aomori_cityname, Iwate_cityname, Miyagi_cityname, Akita_cityname, Yamagata_cityname, Fukushima_cityname, Ibaraki_cityname, Tochigi_cityname, Gunma_cityname, Saitama_cityname, Chiba_cityname, Tokyo_cityname, Kanagawa_cityname, Nigata_cityname, Toyama_cityname, Ishikawa_cityname, Fukui_cityname, Yamanashi_cityname, Nagano_cityname, Gifu_cityname, Shizuoka_cityname, Aichi_cityname, Mie_cityname, Shiga_cityname, Kyoto_cityname, Osaka_cityname, Hyogo_cityname, Nara_cityname, Wakayama_cityname, Tottori_cityname, Shimane_cityname, Okayama_cityname, Hiroshima_cityname, Yamaguchi_cityname, Tokushima_cityname, Kagawa_cityname, Ehime_cityname, Kochi_cityname, Fukuoka_cityname, Saga_cityname, Nagasaki_cityname, Kumamoto_cityname, Oita_cityname, Miyazaki_cityname, Kagoshima_cityname, Okinawa_cityname};
uint8_t cityname_count[] = {16, 3, 3, 2, 2, 4, 3, 2, 2, 2, 3, 3, 4, 2, 4, 2, 2, 2, 2, 3, 2, 4, 2, 2, 2, 2, 1, 2, 2, 2, 2, 3, 2, 2, 4, 2, 1, 3, 3, 4, 2, 4, 4, 4, 4, 4, 7};

char Hokkaido_citycode[][7] =  {"011000", "012010", "012020", "013010", "013020", "013030", "014010", "014020", "014030", "015010", "015020", "016010", "016020", "016030", "017010", "017020"};
char Aomori_citycode[][7] =    {"020010", "020020", "020030"};
char Iwate_citycode[][7] =     {"030010", "030020", "030030"};
char Miyagi_citycode[][7] =    {"040010", "040020"};
char Akita_citycode[][7] =     {"050010", "050020"};
char Yamagata_citycode[][7] =  {"060010", "060020", "060030", "060040"};
char Fukushima_citycode[][7] = {"070010", "070020", "070030"};
char Ibaraki_citycode[][7] =   {"080010", "080020"};
char Tochigi_citycode[][7] =   {"090010", "090020"};
char Gunma_citycode[][7] =     {"100010", "100020"};
char Saitama_citycode[][7] =   {"110010", "110020", "110030"};
char Chiba_citycode[][7] =     {"120010", "120020", "120030"};
char Tokyo_citycode[][7] =     {"130010", "130020", "130030", "130040"};
char Kanagawa_citycode[][7] =  {"140010", "140020"};
char Nigata_citycode[][7] =    {"150010", "150020", "150030", "150040"};
char Toyama_citycode[][7] =    {"160010", "160020"};
char Ishikawa_citycode[][7] =  {"170010", "170020"};
char Fukui_citycode[][7] =     {"180010", "180020"};
char Yamanashi_citycode[][7] = {"190010", "190020"};
char Nagano_citycode[][7] =    {"200010", "200020", "200030"};
char Gifu_citycode[][7] =      {"210010", "210020"};
char Shizuoka_citycode[][7] =  {"220010", "220020", "220030", "220040"};
char Aichi_citycode[][7] =     {"230010", "230020"};
char Mie_citycode[][7] =       {"240010", "240020"};
char Shiga_citycode[][7] =     {"250010", "250020"};
char Kyoto_citycode[][7] =     {"260010", "260020"};
char Osaka_citycode[][7] =     {"270000"};
char Hyogo_citycode[][7] =     {"280010", "280020"};
char Nara_citycode[][7] =      {"290010", "290020"};
char Wakayama_citycode[][7] =  {"300010", "300020"};
char Tottori_citycode[][7] =   {"310010", "310020"};
char Shimane_citycode[][7] =   {"320010", "320020", "320030"};
char Okayama_citycode[][7] =   {"330010", "330020"};
char Hiroshima_citycode[][7] = {"340010", "340020"};
char Yamaguchi_citycode[][7] = {"350010", "350020", "350030", "350040"};
char Tokushima_citycode[][7] = {"360010", "360020"};
char Kagawa_citycode[][7] =    {"370000"};
char Ehime_citycode[][7] =     {"380010", "380020", "380030"};
char Kochi_citycode[][7] =     {"390010", "390020", "390030"};
char Fukuoka_citycode[][7] =   {"400010", "400020", "400030", "400040"};
char Saga_citycode[][7] =      {"410010", "410020"};
char Nagasaki_citycode[][7] =  {"420010", "420020", "420030", "420040"};
char Kumamoto_citycode[][7] =  {"430010", "430020", "430030", "430040"};
char Oita_citycode[][7] =      {"440010", "440020", "440030", "440040"};
char Miyazaki_citycode[][7] =  {"450010", "450020", "450030", "450040"};
char Kagoshima_citycode[][7] = {"460010", "460020", "460030", "460040"};
char Okinawa_citycode[][7] =   {"471010", "471020", "471030", "472000", "473000", "474010", "474020"};

char (*citycodes[])[7] = {Hokkaido_citycode, Aomori_citycode, Iwate_citycode, Miyagi_citycode, Akita_citycode, Yamagata_citycode, Fukushima_citycode, Ibaraki_citycode, Tochigi_citycode, Gunma_citycode, Saitama_citycode, Chiba_citycode, Tokyo_citycode, Kanagawa_citycode, Nigata_citycode, Toyama_citycode, Ishikawa_citycode, Fukui_citycode, Yamanashi_citycode, Nagano_citycode, Gifu_citycode, Shizuoka_citycode, Aichi_citycode, Mie_citycode, Shiga_citycode, Kyoto_citycode, Osaka_citycode, Hyogo_citycode, Nara_citycode, Wakayama_citycode, Tottori_citycode, Shimane_citycode, Okayama_citycode, Hiroshima_citycode, Yamaguchi_citycode, Tokushima_citycode, Kagawa_citycode, Ehime_citycode, Kochi_citycode, Fukuoka_citycode, Saga_citycode, Nagasaki_citycode, Kumamoto_citycode, Oita_citycode, Miyazaki_citycode, Kagoshima_citycode, Okinawa_citycode};

int8_t SelectPrefIndex;
int8_t SelectCityIndex;

String SelectCityName;
String TodayTenki;
String TodayTemperatureMax;
String TodayTemperatureMin;
String TomorrowTenki;
String TomorrowTemperatureMax;
String TomorrowTemperatureMin;

uint8_t tenkiScrollMax = 3;
String tenkiResBody = "";
