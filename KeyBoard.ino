#define keytop_count 52
const uint8_t keytop_height = 9;
bool isLower = true;
int8_t keyX = 0;
int8_t keyY = 0;

String keyboardStr = "";

char keytop_lower[][1] = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '^', '\\', //BS
                          'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '@', '[',  //Enter
                          'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', ':', ']', // Enter
                          'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', '\\' //Cancel
                         };

char keytop_upper[][1] = {'!', '\"', '#', '$', '%', '&', '\'', '(', ')', ' ', '=', '~', '|',
                          'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '`', '{',
                          'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', '+', '*', '}',
                          'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?', '_'
                         };
//
uint8_t keytop_left[] = {  9, 18, 27, 36, 45, 54, 63, 72, 81, 90,   99, 108, 117, //13
                           14, 23, 32, 41, 50, 59, 68, 77, 86, 95,  104, 113,      //25
                           16, 25, 34, 43, 52, 61, 70, 79, 88, 97,  106, 115,      //37
                           21, 30, 39, 48, 57, 66, 75, 84, 93, 102, 111,           //48
                        };

void drawKeyboard(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  char drawStr[2] = " ";
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_10);
  //display->drawLine(x + 127 , y + 16 + 8 , x + 127 , y + 16 + 8 + 47);

  //display->drawLine(x + 0 , y + 16 + 8 , x + 127 , y + 16 + 8);
  //display->drawLine(x + 0 , y + 16 + 8 + 1 , x + 0 , y + 16 + 8 + 9);
  for (int i = 0; i < 13; i++) {
    //display->drawLine(x + keytop_left[i] , y + 16 + 8 + 1 , x + keytop_left[i] , y + 16 + 8 + 9);
    drawStr[0] = isLower ? keytop_lower[i][0] : keytop_upper[i][0];
    display->drawString(x + keytop_left[i] + 5 - 9, y + 15 + 8, drawStr);
  }

  //display->drawLine(x + 0 , y + 16 + 8 + 10 , x + 127 , y + 16 + 8 + 10);
  //display->drawLine(x + 5 , y + 16 + 8 + 1 + 10 , x + 5 , y + 16 + 8 + 9 + 10);
  for (int i = 13; i < 25; i++) {
    //display->drawLine(x + keytop_left[i] , y + 16 + 8 + 1 + 10 , x + keytop_left[i] , y + 16 + 8 + 9 + 10);
    drawStr[0] = isLower ? keytop_lower[i][0] : keytop_upper[i][0];
    display->drawString(x + keytop_left[i] + 5 - 9, y + 15 + 8 + 10, drawStr);
  }

  //display->drawLine(x + 5 , y + 16 + 8 + 20 , x + 115 , y + 16 + 8 + 20);
  //display->drawLine(x + 7 , y + 16 + 8 + 1 + 20 , x + 7 , y + 16 + 8 + 9 + 20);
  for (int i = 25; i < 37; i++) {
    //display->drawLine(x + keytop_left[i] , y + 16 + 8 + 1 + 20 , x + keytop_left[i] , y + 16 + 8 + 9 + 20);
    drawStr[0] = isLower ? keytop_lower[i][0] : keytop_upper[i][0];
    display->drawString(x + keytop_left[i] + 5 - 9, y + 15 + 8 + 20, drawStr);
  }

  //display->drawLine(x + 0 , y + 16 + 8 + 1 + 30 , x + 0 , y + 16 + 8 + 47);

  //display->drawLine(x + 0 , y + 16 + 8 + 30 , x + 127 , y + 16 + 8 + 30);
  //display->drawLine(x + 12 , y + 16 + 8 + 1 + 30 , x + 12 , y + 16 + 8 + 9 + 30);
  for (int i = 37; i < 48; i++) {
    //display->drawLine(x + keytop_left[i] , y + 16 + 8 + 1 + 30 , x + keytop_left[i] , y + 16 + 8 + 9 + 30);
    drawStr[0] = isLower ? keytop_lower[i][0] : keytop_upper[i][0];
    display->drawString(x + keytop_left[i] + 5 - 9, y + 15 + 8 + 30, drawStr);
  }
  //BackSpace
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->drawString(x + 117 + 1, y + 15 + 8, "<");
  display->drawString(x + 117 + 3, y + 15 + 8, "=");
  display->drawString(x + 117 + 3, y + 15 + 8 + 2, "'");
  display->drawString(x + 117 + 5, y + 15 + 8 + 2, "'");
  display->drawString(x + 117 + 7, y + 15 + 8 + 2, "'");
  display->drawString(x + 117 + 4, y + 15 + 8 + 1, "'");
  display->drawString(x + 117 + 4, y + 15 + 8 + 3, "'");
  display->drawString(x + 117 + 5, y + 15 + 8, "'");
  display->drawString(x + 117 + 5, y + 15 + 8 + 4, "'");
  //Enter
  display->drawString(x + 117, y + 15 + 8 + 15, "<");
  display->drawString(x + 117 + 2, y + 15 + 8 + 15 - 1, "-");
  display->drawString(x + 117 + 4, y + 15 + 8 + 15 - 1, "-");
  display->drawString(x + 117 + 5, y + 15 + 8 + 15 - 1, "-");
  display->drawString(x + 117 + 7, y + 15 + 8 + 15, "'");
  //Cancel
  display->drawString(x + 117 + 3, y + 15 + 8 + 28, "x");

  //underbar
  if (!isLower) {
    display->drawString(x + 111 - 5, y + 15 + 8 + 30 + 2, "-");
  }

  //Shift
  if (isLower) {
    display->drawString(x + 6, y + 15 + 8 + 28 + 5, "'");
    display->drawString(x + 4, y + 15 + 8 + 28 + 5, "'");
    display->drawString(x + 5, y + 15 + 8 + 28 + 3, "-");
    display->drawString(x + 5, y + 15 + 8 + 28 + 1, "-");
    display->drawString(x + 2, y + 15 + 8 + 28 + 1, "-");
    display->drawString(x + 7, y + 15 + 8 + 28 + 1, "-");
    display->drawString(x + 3, y + 15 + 8 + 28, "-");
    display->drawString(x + 6, y + 15 + 8 + 28, "-");
    display->drawString(x + 5, y + 15 + 8 + 28 + 2, "'");
    display->drawString(x + 4, y + 15 + 8 + 28 + 3, "'");
    display->drawString(x + 6, y + 15 + 8 + 28 + 3, "'");
  } else {
    display->drawString(x + 6, y + 15 + 8 + 28 + 2, "'");
    display->drawString(x + 4, y + 15 + 8 + 28 + 2, "'");
    display->drawString(x + 5, y + 15 + 8 + 28 - 2, "-");
    display->drawString(x + 5, y + 15 + 8 + 28, "-");
    display->drawString(x + 2, y + 15 + 8 + 28, "-");
    display->drawString(x + 7, y + 15 + 8 + 28, "-");
    display->drawString(x + 3, y + 15 + 8 + 28 + 1, "-");
    display->drawString(x + 6, y + 15 + 8 + 28 + 1, "-");
    display->drawString(x + 5, y + 15 + 8 + 28 + 5, "'");
    display->drawString(x + 4, y + 15 + 8 + 28 + 4, "'");
    display->drawString(x + 6, y + 15 + 8 + 28 + 4, "'");
  }

  if (keyY == 0 && keyX < 13) {
    display->drawRect(x + keytop_left[keyX + 0] - 9, y + 16 + 8, 10, 11);
  } else if (keyY == 1 && keyX < 12) {
    display->drawRect(x + keytop_left[keyX + 13] - 9, y + 16 + 8 + 10, 10, 11);
  } else if (keyY == 2 && keyX < 12) {
    display->drawRect(x + keytop_left[keyX + 25] - 9, y + 16 + 8 + 20, 10, 11);
  } else if (keyY == 3 && keyX < 11) {
    display->drawRect(x + keytop_left[keyX + 37] - 9, y + 16 + 8 + 30, 10, 10);
  } else if (keyY == 0 && keyX == 13) {//BackSpace
    display->drawRect(x + 117, y + 16 + 8, 11, 11);
  } else if (keyY == 1 && keyX == 12) {//Enter
    display->drawRect(x + 115, y + 16 + 8 + 10, 13, 21);
  } else if (keyY == 3 && keyX == 11) {//Cancel
    display->drawRect(x + 117, y + 16 + 8 + 30, 11, 10);
  } else if (keyY == 3 && keyX == 12) {//Shift
    display->drawRect(x + 0, y + 16 + 8 + 30, 11, 10);
  }
  //display->drawLine(x + 0 , y + 16 + 8 + 40 , x + 127 , y + 16 + 8 + 40);

  //display->drawLine(x + 0 , y + 16 + 8 + 47 , x + 127 , y + 16 + 8 + 47);

  //show input
  display->drawString(x + 0, y + 13, keyboardStr + (millis() % 1000 < 500 ? "|" : " "));
}

