//メニュー
uint8_t selectedItemIndex = 0;
uint8_t viewableItemIndex = 0;
#define topMenuItems_Count 3
char topMenuItems[][25] = {"天気予報", "アナログ時計", "設定"};
#define settingMenuItems_Count 3
char settingMenuItems[][25] = {"Wi-Fi設定", "画面焼き付き解消", "方向キーテスト"};
#define wifiMenuItems_Count 2
char wifiMenuItems[][25] = {"Wi-Fi情報", "Wi-Fi接続先設定"};
#define wifiInfoMenuItems_Count 14
char wifiInfoMenuItems[][25] = {"SSID:", "", "IPアドレス:", "", "サブネット:", "", "ゲートウェイ:", "", "DNS:", "", "電波強度(dBm):", "", "状態:", ""};
uint8_t wifiConnectionMenuItems_Count = 9;
char wifiConnectionMenuItems[][25] = {"接続先を追加する", "", "", "", "", "", "", "", ""};
#define wifiConnectionEditMenuItems_Count 1
char wifiConnectionEditMenuItems[][25] = {"接続先設定を削除"};
uint8_t wifiConnectionScanMenuItems_Count = 1;
char wifiConnectionScanMenuItems[][25] = {"", "", "", "", "", "", "", ""};

uint8_t tenkiSettingMenuItems_Count = 47;
char tenkiSettingMenuItems[][25] = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};

char (*currentMenuItems)[25];
uint8_t allItemsCount = topMenuItems_Count;

long lastErrorMillis = 0;
