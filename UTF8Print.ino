
uint8_t checkPattern[16] = {
  B10101010,
  B01010101,
  B10101010,
  B01010101,
  B10101010,
  B01010101,
  B10101010,
  B01010101,
  B10101010,
  B01010101,
  B10101010,
  B01010101,
  B10101010,
  B01010101,
  B10101010,
  B01010101,
};

void draw8x16buffer(OLEDDisplay *display, int16_t xMove, int16_t yMove, char *data, OLEDDISPLAY_COLOR color) {

  uint16_t i;
  int8_t yOffset = yMove & 7;
  for (i = 0; i < 8; i++) {
    int16_t dataPos    = xMove + 7 - i  + (yMove >> 3) * DISPLAY_WIDTH;
    switch (color) {
      case WHITE:   display->buffer[dataPos] |= data[i] << yOffset; break;
      case BLACK:   display->buffer[dataPos] &= data[i] << yOffset; break;
      case INVERSE: display->buffer[dataPos] ^= data[i] << yOffset; break;
    }
    yield();
  }
  for (i = 8; i < 16; i++) {
    int16_t dataPos    = xMove + (7 - (i - 8))  + ((yMove + 8) >> 3) * DISPLAY_WIDTH;
    switch (color) {
      case WHITE:   display->buffer[dataPos] |= data[i] << yOffset; break;
      case BLACK:   display->buffer[dataPos] &= data[i] << yOffset; break;
      case INVERSE: display->buffer[dataPos] ^= data[i] << yOffset; break;
    }
    yield();
  }
}

uint8_t fontCacheCount = 0;
uint32_t cacheUcs2[256];
char cacheFont[256][32];
void FontRead(uint16_t ucs2, char* ret) {
  int i;
  for (i = 0; i < 256; i++) {
    if (cacheUcs2[i] == ucs2) {
      memcpy(ret, cacheFont[i], 32);
      //Serial.println("Using Font Cache");
      return;
    }
  }
  char FontFile[13] = "";
  sprintf(FontFile, "/Font%03d.bin", (ucs2 / 256));
  File f1 = SPIFFS.open(FontFile, "r"); //東雲フォントファイル読み込み
  if (f1) {
    f1.seek(((uint32_t)ucs2 % 256) * 32, SeekSet);
    f1.readBytes(ret, 32); //16bit*16
    f1.close();
    cacheUcs2[fontCacheCount] = ucs2;
    memcpy(cacheFont[fontCacheCount++], ret, 32);
    //Serial.println("Load Font File");
    return;
  } else {
    Serial.println(F("Font.bin file has not been uploaded to the flash in SPIFFS file system"));
  }

}

uint8_t PutUChar(OLEDDisplay *display, uint16_t ucs2, uint8_t x, uint8_t y, OLEDDISPLAY_COLOR color) {
  char c[32];
  //unsigned long t1,t2;
  //Serial.printf("ucs2:%04X column:%02d row:%02d\n",ucs2,column,row);
  //t1 = millis();
  FontRead(ucs2, c);
  if (memcmp(c, checkPattern, 16) == 0 ) {
    draw8x16buffer(display, x, y, c + 16, color);
    return 8;
  } else {
    draw8x16buffer(display, x, y, c, color);
    draw8x16buffer(display, x + 8, y, c + 16, color);
    return 16;
  }
}

void PrintUtf8(OLEDDisplay *display, String text, uint8_t x, uint8_t y, OLEDDISPLAY_COLOR color) {
  //int x = this->Coord.x;
  uint16_t ucs2 = 0;
  uint8_t c;
  uint8_t startx = x;
  const char* str = text.c_str();
  while ( (c = *str) != 0) {
    if ( c <= 0x7F )               // first byte = 0xxxxxxx
    {
      if (c == '\n')
      {
        str++;
      }
      else
      {
        ucs2 = c;
        str++;
        startx += PutUChar(display, ucs2, startx, y, color);
      }
    }
    else if ( (c >= 0xC0) && (c <= 0xDF) )    // 2byte char
    {
      ucs2 = (c & 0x1F) << 6;
      str++;
      ucs2 += (*str++ & 0x3F);
      startx += PutUChar(display, ucs2, startx, y, color);
    }
    else if ( (c >= 0xE0) && (c <= 0xEF) )    // 3 byte char (Kanji)
    {
      ucs2 = (c & 0x0F) << 12;
      str++;
      ucs2 += (*str++ & 0x3F) << 6;
      ucs2 += (*str++ & 0x3F);
      startx += PutUChar(display, ucs2, startx, y, color);
    }
  }
}
