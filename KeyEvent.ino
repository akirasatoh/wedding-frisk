//スイッチ監視

enum KEYS {KEY_RIGHT, KEY_UP, KEY_LEFT, KEY_DOWN, KEY_ENTER};

bool old_keys[5];
bool keys[5];
#define keyboard_wifi 0
int currentKeyboard = keyboard_wifi;

void KeyBoard_KeyDown(uint8_t key);
void Menu_KeyDown(uint8_t key);
void ArrowKeyTest_KeyDown(uint8_t key);

typedef void (*KeyDownEvent)(uint8_t key);
KeyDownEvent Current_KeyDownEvent = Menu_KeyDown;

int8_t tmpSelectPrefIndex = 0;


void changeMenu(int selected, int viewable, int allcount, char* title, char (*menuItems)[25]) {
  selectedItemIndex = selected;
  viewableItemIndex = viewable;
  allItemsCount = allcount;
  strncpy(PageTitle, title, 64);
  currentMenuItems = menuItems;
}
//キーボードのキーイベント
void KeyBoard_KeyDown(uint8_t key) {
  switch (key) {
    case KEY_UP:
      if (keyY == 1 && keyX == 12) {
        keyX = 13;
      } else if (keyY == 0 && keyX == 13) {
        keyX = 11;
        keyY = 4;
      } else if (keyY == 3 && keyX == 11) {
        keyX = 12;
      }
      keyY--;
      break;
    case KEY_DOWN:
      if (keyY == 1 && keyX == 12) {
        keyX = 11;
        keyY = 2;
      }
      else if (keyY == 0 && keyX == 13) keyX = 12;
      keyY++;
      break;
    case KEY_LEFT:
      keyX--;
      break;
    case KEY_RIGHT:
      keyX++;
      break;
    case KEY_ENTER:
      if (keyY == 0 && keyX == 13) {//BackSpace
        if (keyboardStr.length() > 0) keyboardStr.remove(keyboardStr.length() - 1);
      } else if (keyY == 1 && keyX == 12) {//Enter
        if (currentKeyboard == keyboard_wifi) {
          frames[0] = Wait_Frame;
          strncpy(PageTitle, "接続中...", 64);
          ui.update();
          if (connectWiFi(WiFi.SSID(selectedItemIndex), keyboardStr)) {
            frames[0] = Wait_Frame;
            strncpy(PageTitle, "処理中...", 64);
            ui.update();
            removeWiFi(ssidList[wifiConnectionMenuItems_Count]);
            getSavedWiFiList();
            int i = 0;
            for (i = 0; i < ssidCount ; i++) {
              snprintf(wifiConnectionMenuItems[i], 25, "%s", ssidList[i].c_str());
            }
            if (ssidCount < 8) snprintf(wifiConnectionMenuItems[ssidCount] , 25, "%s", "接続先を追加する");
            wifiConnectionMenuItems_Count = ssidCount + 1;
            changeMenu(0, 0, wifiConnectionMenuItems_Count, "接続先", wifiConnectionMenuItems);
            frames[0] = Menu_Frame;
            Current_KeyDownEvent = Menu_KeyDown;
            isWifiConnected = true;
            ntp_begin(2390);
          } else {
            lastErrorMillis = millis();
            overlays[0] = ErrorOverlay;

            frames[0] = Input_Frame;
            strncpy(PageTitle, "ﾊﾟｽﾜｰﾄﾞ:", 64);
            Current_KeyDownEvent = KeyBoard_KeyDown;
          }
        }
      } else if (keyY == 3 && keyX == 11) {//Cancel
        if (currentKeyboard == keyboard_wifi) {
          frames[0] = Wait_Frame;
          strncpy(PageTitle, "検索中...", 64);
          ui.update();
          int n = searchWiFi();
          int i;
          for (i = 0; i < n && i <= 8; i++) {
            snprintf(wifiConnectionScanMenuItems[i], 25, "%s", WiFi.SSID(i).c_str());
          }
          wifiConnectionScanMenuItems_Count = i;
          changeMenu(0, 0, wifiConnectionScanMenuItems_Count, "スキャン", wifiConnectionScanMenuItems);
          frames[0] = Menu_Frame;
          Current_KeyDownEvent = Menu_KeyDown;
        }
      } else if (keyY == 3 && keyX == 12) {//Shift
        isLower = !isLower;
      } else { //ascii keys
        if (keyY == 0) {
          keyboardStr += String(isLower ? keytop_lower[keyX + 0][0] : keytop_upper[keyX + 0][0]);
        } else if (keyY == 1) {
          keyboardStr += String(isLower ? keytop_lower[keyX + 13][0] : keytop_upper[keyX + 13][0]);
        } else if (keyY == 2) {
          keyboardStr += String(isLower ? keytop_lower[keyX + 25][0] : keytop_upper[keyX + 25][0]);
        } else if (keyY == 3) {
          keyboardStr += String(isLower ? keytop_lower[keyX + 37][0] : keytop_upper[keyX + 37][0]);
        }
      }
      break;
  }

  if (keyY < 0) {
    keyY = 3;
    if (keyX > 11) keyX = 11;
  } else if (keyY > 3) {
    keyY = 0;
  }
  if (keyX < 0)  {
    if (keyY == 0) {
      keyX = 13;
    } else if (keyY == 1) {
      keyX = 12;
    } else if (keyY == 2) {
      keyX = 12;
      keyY = 1;
    } else if (keyY == 3) {
      keyX = 12;
    }
  }
  if (keyY == 2 && keyX == 12) {
    keyX = 12;
    keyY = 1;
  }
  if (keyY == 0 && keyX > 13) {
    keyX = 0;
  } else if (keyY == 1 && keyX > 12) {
    keyX = 0;
  } else if (keyY == 2 && keyX > 12) {
    keyX = 0;
  } else if (keyY == 3 && keyX > 12) {
    keyX = 0;
  }
}


//メニューのキーイベント
void Menu_KeyDown(uint8_t key) {
  switch (key) {
    case KEY_UP:
      if (selectedItemIndex > 0) selectedItemIndex--;
      if (selectedItemIndex < viewableItemIndex) viewableItemIndex = selectedItemIndex;
      break;
    case KEY_DOWN:
      if (allItemsCount - 1 > selectedItemIndex) selectedItemIndex++;
      if (selectedItemIndex - 2 > viewableItemIndex) viewableItemIndex = selectedItemIndex - 2;
      break;
    case KEY_LEFT:
      if (currentMenuItems == settingMenuItems) { //設定メニュー
        changeMenu(2, 0, topMenuItems_Count, "ﾒｲﾝﾒﾆｭｰ", topMenuItems);
      } else if (currentMenuItems == wifiMenuItems) { //Wi-Fi設定メニュー
        doWiFiConnect = 0;
        changeMenu(0, 0, settingMenuItems_Count, "設定ﾒﾆｭｰ", settingMenuItems);
      } else if (currentMenuItems == wifiInfoMenuItems) { //Wi-Fi情報メニュー
        changeMenu(0, 0, wifiMenuItems_Count, "Wi-Fi設定", wifiMenuItems);
      } else if (currentMenuItems == wifiConnectionMenuItems) { //Wi-Fi情報メニュー
        changeMenu(1, 0, wifiMenuItems_Count, "Wi-Fi設定", wifiMenuItems);
      } else if (currentMenuItems == wifiConnectionEditMenuItems) {
        int oldindex = wifiConnectionMenuItems_Count;
        int i = 0;
        for (i = 0; i < ssidCount ; i++) {
          snprintf(wifiConnectionMenuItems[i], 25, "%s", ssidList[i].c_str());
        }
        if (ssidCount < 8) snprintf(wifiConnectionMenuItems[ssidCount] , 25, "%s", "接続先を追加する");
        wifiConnectionMenuItems_Count = ssidCount + 1;
        int tmp = 0;
        if (oldindex - 2 > tmp) tmp = oldindex - 2;
        changeMenu(oldindex, 0, wifiConnectionMenuItems_Count, "接続先", wifiConnectionMenuItems);
      } else if (currentMenuItems == wifiConnectionScanMenuItems) { //Wi-Fiスキャン結果メニュー
        int tmp = 0;
        if (wifiConnectionMenuItems_Count - 1 - 2 > tmp) tmp = wifiConnectionMenuItems_Count - 1 - 2;
        changeMenu(wifiConnectionMenuItems_Count - 1, tmp, wifiConnectionMenuItems_Count, "接続先", wifiConnectionMenuItems);
      } else if (currentMenuItems == tenkiSettingMenuItems) { //天気地域設定
        if (tenkiSettingMenuItems_Count == 47) { //都道府県選択
          tenkiLoadCity();
          tenkiDownloadData();
          Current_KeyDownEvent = Tenki_KeyDown;
          frames[0] = Tenki_Frame;
          selectedItemIndex = 0;
          strncpy(PageTitle, SelectCityName.c_str(), 64);
        } else { //詳細地域選択
          frames[0] = Menu_Frame;
          Current_KeyDownEvent = Menu_KeyDown;
          tenkiSettingMenuItems_Count = 47;
          for (int i = 0; i < tenkiSettingMenuItems_Count; i++) {
            strncpy(tenkiSettingMenuItems[i], pref_name[i], 25);
          }
          int tmp = 0;
          if (tmpSelectPrefIndex - 2 > tmp) tmp = tmpSelectPrefIndex - 2;
          changeMenu(tmpSelectPrefIndex, tmp, tenkiSettingMenuItems_Count, "地域選択", tenkiSettingMenuItems);
        }
      }
      break;
    case KEY_RIGHT:
    case KEY_ENTER:
      if (currentMenuItems == topMenuItems) { //トップメニュー
        switch (selectedItemIndex) {
          case 0: //天気予報
            tenkiLoadCity();
            tenkiDownloadData();
            Current_KeyDownEvent = Tenki_KeyDown;
            frames[0] = Tenki_Frame;
            strncpy(PageTitle, SelectCityName.c_str(), 64);
            selectedItemIndex = 0;
            break;
          //case 1: //電車遅延情報

          //  break;
          case 1: //アナログ時計
            Current_KeyDownEvent = analogClock_KeyDown;
            frames[0] = analogClock_Frame;
            strncpy(PageTitle, "ｱﾅﾛｸﾞ時計", 64);
            break;
          //case 3: //テトリス

          //  break;
          case 2: //設定
            changeMenu(0, 0, settingMenuItems_Count, "設定ﾒﾆｭｰ", settingMenuItems);
            break;
        }
      } else if (currentMenuItems == settingMenuItems) { //設定メニュー
        switch (selectedItemIndex) {
          case 0: //Wi-Fi設定
            doWiFiConnect = 2;
            changeMenu(0, 0, wifiMenuItems_Count, "Wi-Fi設定", wifiMenuItems);
            break;
          case 1: //画面焼き付き解消
            Current_KeyDownEvent = OLEDRepair_KeyDown;
            overlays[0] = NoOverlay;
            frames[0] = OLEDRepair_Frame;
            break;
          case 2: //方向キーテスト
            Current_KeyDownEvent = ArrowKeyTest_KeyDown;
            frames[0] = SwitchTest_Frame;
            strncpy(PageTitle, "方向ｷｰﾃｽﾄ", 64);
            break;
        }
      } else if (currentMenuItems == wifiMenuItems) { //Wi-Fi設定メニュー
        switch (selectedItemIndex) {
          case 0: //Wi-Fi情報
            snprintf(wifiInfoMenuItems[1], 25, "%s", WiFi.SSID().c_str());
            snprintf(wifiInfoMenuItems[3], 25, "%s", WiFi.localIP().toString().c_str());
            snprintf(wifiInfoMenuItems[5], 25, "%s", WiFi.subnetMask().toString().c_str());
            snprintf(wifiInfoMenuItems[7], 25, "%s", WiFi.gatewayIP().toString().c_str());
            snprintf(wifiInfoMenuItems[9], 25, "%s", WiFi.dnsIP().toString().c_str());
            snprintf(wifiInfoMenuItems[11], 25, "%ld", WiFi.RSSI());
            snprintf(wifiInfoMenuItems[13], 25, "%s", statusWiFiStr().c_str());
            changeMenu(0, 0, wifiInfoMenuItems_Count, "Wi-Fi情報", wifiInfoMenuItems);
            break;
          case 1: //Wi-Fi接続先設定
            {
              int i = 0;
              for (i = 0; i < ssidCount ; i++) {
                snprintf(wifiConnectionMenuItems[i], 25, "%s", ssidList[i].c_str());
              }
              if (ssidCount < 8) snprintf(wifiConnectionMenuItems[ssidCount] , 25, "%s", "接続先を追加する");
              wifiConnectionMenuItems_Count = ssidCount + 1;
              changeMenu(0, 0, wifiConnectionMenuItems_Count, "接続先", wifiConnectionMenuItems);
              break;
            }
        }
      } else if (currentMenuItems == wifiConnectionMenuItems) { //Wi-Fi接続先メニュー
        if (selectedItemIndex == ssidCount) { //接続先を追加する
          frames[0] = Wait_Frame;
          strncpy(PageTitle, "検索中...", 64);
          ui.update();
          int n = searchWiFi();
          int i;
          for (i = 0; i < n && i <= 8; i++) {
            snprintf(wifiConnectionScanMenuItems[i], 25, "%s", WiFi.SSID(i).c_str());
          }
          wifiConnectionScanMenuItems_Count = i;
          changeMenu(0, 0, wifiConnectionScanMenuItems_Count, "スキャン", wifiConnectionScanMenuItems);
          frames[0] = Menu_Frame;
        } else {
          wifiConnectionMenuItems_Count = selectedItemIndex;
          char title[10];
          snprintf(title, 9, "%s", ssidList[selectedItemIndex].c_str());
          changeMenu(0, 0, wifiConnectionEditMenuItems_Count, title, wifiConnectionEditMenuItems);
        }
      } else if (currentMenuItems == wifiConnectionScanMenuItems) { //スキャンしたWi-Fi選択メニュー
        //selectedItemIndex
        frames[0] = Input_Frame;
        strncpy(PageTitle, "ﾊﾟｽﾜｰﾄﾞ:", 64);
        Current_KeyDownEvent = KeyBoard_KeyDown;
      } else if (currentMenuItems == wifiConnectionEditMenuItems) { //接続先編集
        frames[0] = Wait_Frame;
        strncpy(PageTitle, "削除中...", 64);
        ui.update();
        removeWiFi(ssidList[wifiConnectionMenuItems_Count]);
        getSavedWiFiList();
        int i = 0;
        for (i = 0; i < ssidCount ; i++) {
          snprintf(wifiConnectionMenuItems[i], 25, "%s", ssidList[i].c_str());
        }
        if (ssidCount < 8) snprintf(wifiConnectionMenuItems[ssidCount] , 25, "%s", "接続先を追加する");
        wifiConnectionMenuItems_Count = ssidCount + 1;
        changeMenu(0, 0, wifiConnectionMenuItems_Count, "接続先", wifiConnectionMenuItems);
        frames[0] = Menu_Frame;
      } else if (currentMenuItems == tenkiSettingMenuItems) { //天気地域設定
        if (tenkiSettingMenuItems_Count == 47) { //都道府県選択
          tmpSelectPrefIndex = selectedItemIndex;
          tenkiSettingMenuItems_Count = cityname_count[tmpSelectPrefIndex];
          for (int i = 0; i < tenkiSettingMenuItems_Count; i++) {
            strncpy(tenkiSettingMenuItems[i], citynames[tmpSelectPrefIndex][i], 25);
          }
          changeMenu(0, 0, tenkiSettingMenuItems_Count, "地域選択", tenkiSettingMenuItems);
        } else { //詳細地域選択
          SelectPrefIndex = tmpSelectPrefIndex;
          SelectCityIndex = selectedItemIndex;
          tenkiSaveCity();
          tenkiLoadCity();
          tenkiDownloadData();
          Current_KeyDownEvent = Tenki_KeyDown;
          frames[0] = Tenki_Frame;
          selectedItemIndex = 0;
          strncpy(PageTitle, SelectCityName.c_str(), 64);
        }
      }
  }
}

//方向キーテストのキーイベント
void ArrowKeyTest_KeyDown(uint8_t key) {
  switch (key) {
    case KEY_ENTER:
      frames[0] = Menu_Frame;
      Current_KeyDownEvent = Menu_KeyDown;
      strncpy(PageTitle, "設定ﾒﾆｭｰ", 64);
      break;
  }
}

//画面焼き付き解消のキーイベント
void OLEDRepair_KeyDown(uint8_t key) {
  switch (key) {
    case KEY_LEFT:
    case KEY_ENTER:
      frames[0] = Menu_Frame;
      overlays[0] = StatusBarOverlay;
      Current_KeyDownEvent = Menu_KeyDown;
      break;
  }
}

//アナログ時計のキーイベント
void analogClock_KeyDown(uint8_t key) {
  switch (key) {
    case KEY_LEFT:
    case KEY_ENTER:
      frames[0] = Menu_Frame;
      Current_KeyDownEvent = Menu_KeyDown;
      strncpy(PageTitle, "ﾒｲﾝﾒﾆｭｰ", 64);
      break;
  }
}

//天気のキーイベント
void Tenki_KeyDown(uint8_t key) {
  switch (key) {
    case KEY_UP:
      if (selectedItemIndex > 0) selectedItemIndex--;
      break;
    case KEY_DOWN:
      if (selectedItemIndex < tenkiScrollMax - 3) selectedItemIndex++;
      break;
    case KEY_LEFT:
      frames[0] = Menu_Frame;
      Current_KeyDownEvent = Menu_KeyDown;
      changeMenu(0, 0, topMenuItems_Count, "ﾒｲﾝﾒﾆｭｰ", topMenuItems);
      break;
    case KEY_RIGHT:
    case KEY_ENTER:
      frames[0] = Menu_Frame;
      Current_KeyDownEvent = Menu_KeyDown;
      tenkiSettingMenuItems_Count = 47;
      for (int i = 0; i < tenkiSettingMenuItems_Count; i++) {
        strncpy(tenkiSettingMenuItems[i], pref_name[i], 25);
      }
      tmpSelectPrefIndex = SelectPrefIndex;
      int tmp = 0;
      if (tmpSelectPrefIndex - 2 > tmp) tmp = tmpSelectPrefIndex - 2;
      changeMenu(tmpSelectPrefIndex, tmp, tenkiSettingMenuItems_Count, "地域選択", tenkiSettingMenuItems);
      break;
  }
}

void KeyCheck() {
  memcpy(old_keys, keys, sizeof(keys));
  memset(keys, false, sizeof(keys));
  uint16_t adc = system_adc_read();
  if (adc <= 902 && adc > 672) { //→キー
    keys[KEY_RIGHT] = true;
    if (old_keys[KEY_RIGHT] == false) {
      Current_KeyDownEvent(KEY_RIGHT);
    }
  } else if (adc <= 672 && adc > 434) { //↑キー
    keys[KEY_UP] = true;
    if (old_keys[KEY_UP] == false) {
      Current_KeyDownEvent(KEY_UP);
    }
  } else if (adc <= 434 && adc > 152) { //←キー
    keys[KEY_LEFT] = true;
    if (old_keys[KEY_LEFT] == false) {
      Current_KeyDownEvent(KEY_LEFT);
    }
  } else if (adc <= 152) { //↓キー
    keys[KEY_DOWN] = true;
    if (old_keys[KEY_DOWN] == false) {
      Current_KeyDownEvent(KEY_DOWN);
    }
  }

  if (!digitalRead(0)) { //決定キー
    keys[KEY_ENTER] = true;
    if (old_keys[KEY_ENTER] == false) {
      Current_KeyDownEvent(KEY_ENTER);
    }
  }
}

