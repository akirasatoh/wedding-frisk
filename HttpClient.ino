String wget(String url) {
  // URLからhostとRequestPathとportを作る
  String str_host;
  String requestPath;
  uint16_t port;
  makePath(url, &str_host, &requestPath, &port);
  int str_host_len = str_host.length() + 1;
  char host[str_host_len];
  str_host.toCharArray(host, str_host_len);

  Serial.print  ("connecting to ");
  Serial.print(host);
  Serial.print  (":");
  Serial.println(port);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;

  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    return String("");
  }

  Serial.print  ("requestPath: ");
  Serial.println(requestPath);

  // サーバにリクエストを送信
  String requestHeader = makeRequestHeader(host, requestPath);
  //Serial.print(requestHeader);
  client.print(requestHeader);
  while (!client.available()) delay(1);

  // レスポンスを処理
  String resHeader = "";
  String resBody   = "";
  httpGet(&client, &resHeader, &resBody);
  /*
    Serial.println("response header:");
    Serial.println(resHeader);
    Serial.println();
    Serial.print  ("response body:");
    Serial.println(resBody);
    Serial.print  ("Length: ");
    Serial.println(resBody.length());
    Serial.println();
  */
  resBody.trim();
  return resBody;
}

void httpGet(WiFiClient *client, String *resHeader, String *resBody) {

  Serial.println("---httpGet:------------");
  while ((*client).available()) {
    String line = (*client).readStringUntil('\r');
    Serial.print(line);
    if ( line.equals("\n")) {
      break;
    }
    else {
      (*resHeader).concat(line);
    }
  }
  int count = 0;
  //int strlength = 0;
  while ((*client).available()) {
    String line = (*client).readStringUntil('\r');
    count++;
    Serial.print(line);
    if (count % 2 == 01) {
      //line.trim();
      //strlength = line.toInt();
    } else {
      line.trim();
      (*resBody).concat(line);
    }
  }
  Serial.println("-----------------------");
  //(*resBody).remove((*resBody).length() - 4);
}

void makePath(String url, String *host, String *requestPath, uint16_t *port) {
  String urlStr = String(url);
  if ( !urlStr.startsWith("http://") ) {
    Serial.println('URL Parse Error');
  }
  String urlStr2 = urlStr.substring(7);
  uint8_t p = urlStr2.indexOf("/");
  *host = urlStr2.substring(0, p);
  *requestPath = urlStr2.substring(p);
  *port = 80;
}

String makeRequestHeader( String host, String requestPath ) {
  String res = String("GET ") + requestPath + " HTTP/1.1\r\n"
               + "Host: " + host + "\r\n"
               + "User-Agent: ESP8266WiFi.h (+http://cl.hatenablog.com/entry/esp-wroom-02-weather-light)\r\n" +
               + "Connection: close\r\n"
               + "\r\n";
  return res;
}
